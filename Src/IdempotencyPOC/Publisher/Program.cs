﻿using System;
using log4net;
using MassTransit;
using Messages;

namespace Publisher
{
    class Program
    {
        private static ILog _log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
            {
                sbc.Host(new Uri("rabbitmq://localhost"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });
            });

            bus.Start();

            var receiveEndpoint = bus.GetSendEndpoint(new Uri("rabbitmq://localhost/consumer_queue")).Result;

            for (int i = 1; i <= 3; i++)
            {
                receiveEndpoint.Send(new Message(i.ToString(), "UserId"));
            }
            
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();

            bus.Stop();
        }
    }
}
