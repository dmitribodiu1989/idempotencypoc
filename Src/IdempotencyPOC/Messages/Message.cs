﻿using System;

namespace Messages
{
    public class Message
    {
        public Message(string id, string userId)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentException(nameof(id));
            }
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentException(nameof(userId));
            }
            Id = id;
            UserId = userId;
        }
        public string Id { get; }
        public string UserId { get; set; }
    }
}
