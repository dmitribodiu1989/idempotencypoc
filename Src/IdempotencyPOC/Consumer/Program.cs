﻿using System;
using System.Threading;
using System.Threading.Tasks;
using GreenPipes;
using GreenPipes.Configurators;
using MassTransit;
using Messages;

namespace Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            var bus = Bus.Factory.CreateUsingRabbitMq(sbc =>
            {
                sbc.UseConcurrencyLimit(1);
                var host = sbc.Host(new Uri("rabbitmq://localhost"), h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

                sbc.ReceiveEndpoint(host, "consumer_queue", rabbitMqReceiveEndpointConfigurator =>
                {
                    rabbitMqReceiveEndpointConfigurator
                        .UseRetry(configurator => configurator
                            .SetRetryPolicy(filter => filter.Incremental(3, TimeSpan.FromSeconds(2),TimeSpan.FromSeconds(1))));
                    rabbitMqReceiveEndpointConfigurator.Handler<Message>(context =>
                    {
                        Console.WriteLine($"Received: {context.Message.Id}, {context.Message.UserId}, ThreadId : {Thread.CurrentThread.ManagedThreadId}");

                        //if (context.Message.Id == "2")
                        //{
                        //    throw new Exception("Message 2 failed!");
                        //}

                        return Task.CompletedTask;
                    });
                });
            });

            bus.Start();

            Console.ReadKey();
            bus.Stop();
        }
    }
}
